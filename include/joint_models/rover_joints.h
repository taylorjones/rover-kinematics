/*******************************************************************************
* turtlebot_vehicle.h and turtlebot_vehicle.cpp hav been adapted from turtlebot3_fake.h and turtlebot3_fake.cpp
* See the ROBOTIS copyright below:
*
* Copyright 2016 ROBOTIS CO., LTD.
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*     http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*******************************************************************************/

#ifndef ROVER_JOINTS_H_
#define ROVER_JOINTS_H_
#include <math.h>

#include <ros/ros.h>
#include <ros/time.h>
#include <sensor_msgs/JointState.h>
#include <nav_msgs/Odometry.h>

namespace rover_kinematics
{

#define WHEEL_RADIUS                    0.03     // meter
#define WHEEL_BASE_WIDTH                0.14444     // meter
#define WHEEL_BASE_LENGTH               0.17     // meter

#define LEFT_FRONT                            0
#define RIGHT_FRONT                           1
#define LEFT_BACK                             2
#define RIGHT_BACK                            3

class rover_joints
{
public:
    rover_joints();
    ~rover_joints();
    bool init();
    bool update();

private:
    // ROS NodeHandle
    ros::NodeHandle nh_;

    // ROS Topic Publishers
    ros::Publisher joint_states_pub_;

    //ROS SUBSCRIBER
    ros::Subscriber odom_sub_;

    // ROS time for updates
    ros::Time prev_update_time_;

    sensor_msgs::JointState joint_states_;
    double wheel_speed_cmd_[4];

    std::string joint_states_name_[4];

    double last_position_[4];
    double last_velocity_[4];

    void updateJoint(ros::Duration diff_time);
    void odometryCallback(const nav_msgs::OdometryConstPtr odom);

};
} // end namespace turtlebot_sim

#endif // ROVER_JOINTS_H_
