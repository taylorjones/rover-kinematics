#include "joint_models/rover_joints.h"
#include <iostream>
using namespace rover_kinematics;


rover_joints::rover_joints(){
    init();
}
rover_joints::~rover_joints(){

}
bool rover_joints::init(){
    // initialize ROS parameter

    nh_.param("wheel_left_front_joint_name", joint_states_name_[LEFT_FRONT],  std::string("left_front_wheel_joint"));
    nh_.param("wheel_right_front_joint_name", joint_states_name_[RIGHT_FRONT],  std::string("right_front_wheel_joint"));
    nh_.param("wheel_left_back_joint_name", joint_states_name_[LEFT_BACK],  std::string("left_back_wheel_joint"));
    nh_.param("wheel_right_back_joint_name", joint_states_name_[RIGHT_BACK],  std::string("right_back_wheel_joint"));
    nh_.param("joint_states_frame", joint_states_.header.frame_id, std::string("base_link"));

    // initialize variables
    wheel_speed_cmd_[LEFT_FRONT]  = 0.0;
    wheel_speed_cmd_[RIGHT_FRONT] = 0.0;
    wheel_speed_cmd_[LEFT_BACK]   = 0.0;
    wheel_speed_cmd_[RIGHT_BACK]  = 0.0;
    last_position_[LEFT_FRONT]    = 0.0;
    last_position_[RIGHT_FRONT]   = 0.0;
    last_position_[LEFT_BACK]     = 0.0;
    last_position_[RIGHT_BACK]    = 0.0;
    last_velocity_[LEFT_FRONT]    = 0.0;
    last_velocity_[RIGHT_FRONT]   = 0.0;
    last_velocity_[LEFT_BACK]     = 0.0;
    last_velocity_[RIGHT_BACK]    = 0.0;

    joint_states_.name.push_back(joint_states_name_[LEFT_FRONT]);
    joint_states_.name.push_back(joint_states_name_[RIGHT_FRONT]);
    joint_states_.name.push_back(joint_states_name_[LEFT_BACK]);
    joint_states_.name.push_back(joint_states_name_[RIGHT_BACK]);
    joint_states_.position.resize(4,0.0);
    joint_states_.velocity.resize(4,0.0);
    joint_states_.effort.resize(4,0.0);

    // initialize publishers
    joint_states_pub_ = nh_.advertise<sensor_msgs::JointState>("joint_states", 100);
    odom_sub_ = nh_.subscribe("odom", 100, &rover_joints::odometryCallback, this);
    prev_update_time_ = ros::Time::now();
    return true;

}
bool rover_joints::update(){
    ros::Time time_now = ros::Time::now();
    ros::Duration step_time = time_now - prev_update_time_;
    prev_update_time_ = time_now;

    // joint_states
    updateJoint(step_time);
    joint_states_.header.stamp = time_now;
    joint_states_pub_.publish(joint_states_);

    return true;
}

void rover_joints::updateJoint(ros::Duration diff_time){

    double wheel_l_f, wheel_l_b, wheel_r_f, wheel_r_b; // rotation value of wheel [rad]
    double delta_s, delta_theta;
    double v[4], w[4];

    wheel_l_f = wheel_l_b = wheel_r_f = wheel_r_b     = 0.0;
    delta_s = delta_theta = 0.0;

    v[LEFT_FRONT]  = wheel_speed_cmd_[LEFT_FRONT];
    w[LEFT_FRONT]  = v[LEFT_FRONT] / WHEEL_RADIUS;  // w = v / r
    v[RIGHT_FRONT] = wheel_speed_cmd_[RIGHT_FRONT];
    w[RIGHT_FRONT] = v[RIGHT_FRONT] / WHEEL_RADIUS;
    v[LEFT_BACK]  = wheel_speed_cmd_[LEFT_BACK];
    w[LEFT_BACK]  = v[LEFT_BACK] / WHEEL_RADIUS;  // w = v / r
    v[RIGHT_BACK] = wheel_speed_cmd_[RIGHT_BACK];
    w[RIGHT_BACK] = v[RIGHT_BACK] / WHEEL_RADIUS;

    last_velocity_[LEFT_FRONT]  = w[LEFT_FRONT];
    last_velocity_[RIGHT_FRONT] = w[RIGHT_FRONT];
    last_velocity_[LEFT_BACK]  = w[LEFT_BACK];
    last_velocity_[RIGHT_BACK] = w[RIGHT_BACK];

    wheel_l_f = w[LEFT_FRONT]  * diff_time.toSec();
    wheel_r_f = w[RIGHT_FRONT] * diff_time.toSec();
    wheel_l_b = w[LEFT_BACK]  * diff_time.toSec();
    wheel_r_b = w[RIGHT_BACK] * diff_time.toSec();

    if(isnan(wheel_l_f))
    {
      wheel_l_f = 0.0;
    }
    if(isnan(wheel_l_b))
    {
      wheel_l_b = 0.0;
    }
    if(isnan(wheel_r_f))
    {
      wheel_r_f = 0.0;
    }
    if(isnan(wheel_r_b))
    {
      wheel_r_b = 0.0;
    }

    last_position_[LEFT_FRONT]  += wheel_l_f;
    last_position_[RIGHT_FRONT] += wheel_r_f;
    last_position_[LEFT_BACK]  += wheel_l_b;
    last_position_[RIGHT_BACK] += wheel_r_b;


    joint_states_.position[LEFT_FRONT]  = last_position_[LEFT_FRONT];
    joint_states_.position[RIGHT_FRONT] = last_position_[RIGHT_FRONT];
    joint_states_.position[LEFT_BACK]  = last_position_[LEFT_BACK];
    joint_states_.position[RIGHT_BACK] = last_position_[RIGHT_BACK];
    joint_states_.velocity[LEFT_FRONT]  = last_velocity_[LEFT_FRONT];
    joint_states_.velocity[RIGHT_FRONT] = last_velocity_[RIGHT_FRONT];
    joint_states_.velocity[LEFT_BACK]  = last_velocity_[LEFT_BACK];
    joint_states_.velocity[RIGHT_BACK] = last_velocity_[RIGHT_BACK];
}

void rover_joints::odometryCallback(const nav_msgs::OdometryConstPtr odom){
    double linear_velocity_x  = odom->twist.twist.linear.x;
    double linear_velocity_y  = odom->twist.twist.linear.y;
    double angular_velocity = odom->twist.twist.angular.z;
    //std::cout << linear_velocity_x<<", "<<linear_velocity_y<<", "<<angular_velocity<<std::endl;

    wheel_speed_cmd_[LEFT_FRONT]  = (1/WHEEL_RADIUS) * (linear_velocity_x - linear_velocity_y - (WHEEL_BASE_WIDTH + WHEEL_BASE_LENGTH)*angular_velocity);
    wheel_speed_cmd_[LEFT_BACK]  = (1/WHEEL_RADIUS) * (linear_velocity_x + linear_velocity_y - (WHEEL_BASE_WIDTH + WHEEL_BASE_LENGTH)*angular_velocity);
    wheel_speed_cmd_[RIGHT_FRONT] = (1/WHEEL_RADIUS) * (linear_velocity_x + linear_velocity_y + (WHEEL_BASE_WIDTH + WHEEL_BASE_LENGTH)*angular_velocity);
    wheel_speed_cmd_[RIGHT_BACK] = (1/WHEEL_RADIUS) * (linear_velocity_x - linear_velocity_y + (WHEEL_BASE_WIDTH + WHEEL_BASE_LENGTH)*angular_velocity);
}

/*******************************************************************************
* Main function
*******************************************************************************/
int main(int argc, char* argv[])
{
  ros::init(argc, argv, "rover_joints_node");
  rover_joints vehicle;

  ros::Rate loop_rate(30);

  while (ros::ok())
  {
    vehicle.update();
    ros::spinOnce();
    loop_rate.sleep();
  }

  return 0;
}
